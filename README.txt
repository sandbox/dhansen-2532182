Juicebox Inline module
----------------------
by Dan Hansen, theaudiophreak@gmail.com
Sponsored by Southern Spaces and Sevaa Group, Inc.

This module extends the Juicebox module, allowing you to create Juicebox gallery
implementations inline with shortcodes. These shortcodes are rendered with text
filters.

--------------------------------------------------------------------------------
                                  Installation
--------------------------------------------------------------------------------

1. Download and install the Juicebox module. Be sure its dependencies of the
image module and the libraries module are downloaded/enabled.

2. Download and install the Juicebox library. See the Juicebox module's
installation instructions for details.

3. Download and install the Juicebox Inline module in your modules directory.

4. As an administrator, go to Text Format (admin/config/content/formats) and
enable 'Juicebox Inline' on any format where it is intended to be used.

--------------------------------------------------------------------------------
                                     Usage
--------------------------------------------------------------------------------

1. Place images into a text area as you normally would. To work correctly, the
image will need to have an associated record in the site's file system, so no
externally hosted images.

2. Wrap all images in the text area with the [juicebox] shortcode. So your
markup will look something like the following:

[juicebox]
<img src="http://www.yoururl.com/sites/default/files/images/filenameone.jpg" />
<img src="/sites/default/files/images/filenametwo.png" />
[/juicebox]

--------------------------------------------------------------------------------
                                     Notes
--------------------------------------------------------------------------------

* To add a title to the image in the gallery, use the "title" attribute in the
image tag.

* To add a description to the image in the gallery, use the "alt" attribute in
the image tag.

* The Juicebox Inline shortcode will ignore and remove any markup within it and
only use the contents of <img> tags. The only three properties of the <img> tag
that will be used are the src, alt, and title attributes; everything else will
be ignored during rendering.

* The Juicebox Inline shortcode will work with full or partial links to images
on the site, but it uses the src URLs to tie into the file system. Because of
this, externally hosted images and images not within the Drupal filesystem will
not work.

--------------------------------------------------------------------------------
                               Gallery Settings
--------------------------------------------------------------------------------

Juicebox actually has several options to control the display of the gallery,
some of which have been implemented using attributes on the shortcode. For
example, say we wanted the gallery to be 100% wide but maybe only 300px tall and
with a green background. The shortcode would then look like:

[juicebox galleryWidth=“100%” galleryHeight=“300px” backgroundColor=“green”]
…
[/juicebox]

The full list of available settings (and their defaults) is as follows:

galleryWidth
default: 100%
The gallery width in a standard numeric format (such as 100% or 300px).

galleryHeight
default: 100%
The gallery height in a standard numeric format (such as 100% or 300px).

backgroundColor 
default: #FFFFFF
The gallery background color as a CSS3 color value (such as rgba(10,50,100,0.7)
or #FF00FF).

textColor
default: rgba(255,255,255,1)
The color of all gallery text as a CSS3 color value (such as rgba(255,255,255,1)
or #FF00FF).

thumbFrameColor
default: rgba(255,255,255,.5)
The color of the thumbnail frame as a CSS3 color value (such as
rgba(255,255,255,.5) or #FF00FF).

showOpenButton
default: TRUE
Whether to show the "Open Image" button. This will link to the full size version
of the image within a new tab to facilitate downloading.

showExpandButton
default: TRUE
Whether to show the "Expand" button. Clicking this button expands the gallery to
fill the browser window.

showThumbsButton
default: TRUE
Whether to show the "Toggle Thumbnails" button.

useThumbDots
default: FALSE
Whether to replace the thumbnail images with small dots.

useFullscreenExpand
default: FALSE
Whether to trigger fullscreen mode when clicking the expand button (for
supported browsers).

customParentClasses
default: {blank}
Define any custom classes that should be added to the parent container within
the Juicebox embed markup. This can be handy if you want to apply more advanced
styling or dimensioning rules to this gallery via CSS. Enter as space-separated
values.