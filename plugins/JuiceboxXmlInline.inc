<?php


/**
 * @file
 * Juicebox XML loader that's used to load (and build via loaded methods) the
 * XML associated with a Drupal field formatter plugin.
 */


/**
 * Class to load and build the XML associated with a Drupal field formatter
 * plugin.
 */
class JuiceboxXmlInline implements JuiceboxXmlInterface {

  // Injected gallery object to build.
  protected $juicebox;
  // Base properties that reference source data.
  protected $idArgs;
  protected $xmlID;
  // Helper properties for access checks.
  protected $fieldAccess;
  protected $entityAccess;
  // Dynamic loaded data storage.
  protected $settings = array();
  protected $items = array();

  /**
   * Constructor.
   *
   * @param array $id_args
   *   An indexed array of arguments that describe this gallery (and make up its
   *   XML URL). This information uniquely identifies the gallery and contains
   *   all the descriptive data needed to load it.
   */
  public function __construct($id_args) {
    // We need 5 data sources to build a file-based gallery (the type along
    // with 4 identifiers for the field data).
    if (empty($id_args) || count($id_args) < 2) {
      throw new Exception(t('Cannot initiate field-based Juicebox XML due to insufficient ID args.'));
    }
    // Set data sources as properties.
    $this->idArgs = $id_args;
    $this->xmlID = $id_args[1];
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    $access = TRUE;
    // TODO: Figure out how to make access checks when we have no data on the origin
    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function getXml() {
    // Load the data from the array
    $result = db_select('juicebox_inline')
      ->fields('juicebox_inline', array('data'))
      ->condition('xmlid', $this->xmlID, '=')
      ->execute()
      ->fetchField();
    $result = unserialize($result);
    // Initalize a new gallery.
    $juicebox = juicebox();
    $juicebox->init($this->idArgs, $result['display'], $result['items']);
    // Manually build the gallery via the field formatter methods.
    juicebox_inline_build_gallery($juicebox, $result['items']);
    // Render the XML.
    return $juicebox->renderXml();
  }

}
